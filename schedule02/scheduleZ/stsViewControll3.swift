//
//  stsViewControll3.swift
//  scheduleZ
//
//  Created by 中村考男 on 2016/02/27.
//  Copyright © 2016年 tamagawa. All rights reserved.
//


import UIKit

class stsViewControll3: UIViewController, UITableViewDataSource, UITableViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource{
    
    //タイトルラベル
    @IBOutlet weak var lblTittle: UILabel!
    //準備状態ラベル
    @IBOutlet weak var lblPreState: UILabel!
    //作成状態ラベル
    @IBOutlet weak var lblCrtState: UILabel!
    //テスト状態ラベル
    @IBOutlet weak var lblTesState: UILabel!
    //現在工程
    @IBOutlet weak var lblNowProcess: UILabel!
    //現在状態
    @IBOutlet weak var lblNowState: UILabel!
    //現在工程開始日
    @IBOutlet weak var lblNowProcessSdate: UILabel!
    //現在工程終了日
    @IBOutlet weak var lblNowProcessEdate: UILabel!

    @IBOutlet weak var tblProcessInf: UITableView!
    
    var arBaseData = [String]()
    var arDates = [String]()
    var arKoteis = [String]()
    let intLabelTag1 = 1
    
    let arTblProcess = ["準　備","作　成","テスト"]
    var arTblImpProcessMain = [String]()
    var arTblStartDateMain = [String]()
    var arTblEndDateMain = [String]()
    var arTblDaysMain = [String]()
    var arTblStateMain = [String]()
    var arTblHereMain = [String]()
    
    let pvChangeState = UIPickerView()
    var selChangeState = ""
    let arStateItems = ["０％","１０％","２５％","５０％","７５％","９０％","完了！"]
    //更新ボタン
    @IBAction func btnProgressUpdate(sender: AnyObject) {
        //ドキュメントパス
        let path = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
        //ファイル名
        let fileName = "zakkuri.txt"
        //ファイルパス
        let filePath = path + "/" + fileName

        
        //保存配列(基本情報)編集
        //工程と状態の更新
        let strUpdState = jotaiJPtoCD(lblNowState.text!)
        var strUpdPosition :String = koteiJPtoCD(lblNowProcess.text!)!
        
        switch strUpdPosition{
        case "0":
            //工程がテスト以外の場合！
            if strUpdState != "6"{
                //更新状態が完了以外の場合！
                arBaseData[15] = strUpdState!
            }
            else{
                //更新状態が完了の場合！
                arBaseData[15] = strUpdState!
                strUpdPosition  = "1"
            }
        case "1":
            //工程がテスト以外の場合！
            if strUpdState != "6"{
                //更新状態が完了以外の場合！
                arBaseData[16] = strUpdState!
            }
            else{
                //更新状態が完了の場合！
                arBaseData[16] = strUpdState!
                strUpdPosition  = "2"
            }
        case "2":
            //工程がテストの場合！
            arBaseData[17] = strUpdState!
        default: break
        }
        
        arBaseData[5] = strUpdPosition
        print("1")
        
        let FileManager = NSFileManager.defaultManager()
        if FileManager.fileExistsAtPath(filePath){
            //ファイルが存在する場合
            //ファイル削除
            do {
                try NSFileManager.defaultManager().removeItemAtPath(filePath)
            } catch {
                // Failed to write file
            }
            print("2")
            
        }
        // 保存処理
        print("3")

        let result = NSKeyedArchiver.archiveRootObject(arBaseData, toFile: filePath)
        if result{
            print("基本ファイル保存成功")
        }
        //

        loadView()
        viewDidLoad()
        print("viewWillAppear")

    
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        //ラベルタッチ関連
        
        lblNowState.userInteractionEnabled = true
        lblNowState.tag = intLabelTag1
        
        var arTblImpProcess = [String]()
        var arTblStartDate = [String]()
        var arTblEndDate = [String]()
        var arTblDays = [String]()
        var arTblState = [String]()
        var arTblHere = [String]()


        //ドキュメントパス
        let path = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
        //ファイル名
        let fileName = "zakkuri.txt"
        let fileNamedt = "datez.txt"
        let fileNamews = "workSz.txt"
        
        //ファイルパス
        let filePath = path + "/" + fileName
        let filePathdt = path + "/" + fileNamedt
        let filePathws = path + "/" + fileNamews
        
        //既存ファイル存在確認
        let FileManager = NSFileManager.defaultManager()
        if FileManager.fileExistsAtPath(filePath){
            //ファイルが存在する場合
            if let array = NSKeyedUnarchiver.unarchiveObjectWithFile(filePath) as? Array<String>{
                arBaseData = array
            }
        }
        if FileManager.fileExistsAtPath(filePathdt){
            //ファイルが存在する場合
            if let array = NSKeyedUnarchiver.unarchiveObjectWithFile(filePathdt) as? Array<String>{
                arDates = array
            }
        }
        if FileManager.fileExistsAtPath(filePathws){
            //ファイルが存在する場合
            if let array = NSKeyedUnarchiver.unarchiveObjectWithFile(filePathws) as? Array<String>{
                arKoteis = array
            }
        }
        
        switch arBaseData[1] {
        case "0":
            arTblImpProcess.append("重要！")
            arTblImpProcess.append("　")
            arTblImpProcess.append("　")
        case "1":
            arTblImpProcess.append("　")
            arTblImpProcess.append("重要！")
            arTblImpProcess.append("　")
        case "2":
            arTblImpProcess.append("　")
            arTblImpProcess.append("　")
            arTblImpProcess.append("重要！")
        default: break
        }

        arTblStartDate.append(arBaseData[6])
        arTblStartDate.append(arBaseData[8])
        arTblStartDate.append(arBaseData[10])
        
        arTblEndDate.append(arBaseData[7])
        arTblEndDate.append(arBaseData[9])
        arTblEndDate.append(arBaseData[11])
        
        arTblDays.append(arBaseData[12])
        arTblDays.append(arBaseData[13])
        arTblDays.append(arBaseData[14])
        
        arTblState.append(jotaiCDtoJP(arBaseData[15])!)
        arTblState.append(jotaiCDtoJP(arBaseData[16])!)
        arTblState.append(jotaiCDtoJP(arBaseData[17])!)
        
        switch arBaseData[5] {
        case "0":
            arTblHere.append("Now!")
            arTblHere.append("　")
            arTblHere.append("　")
        case "1":
            arTblHere.append("　")
            arTblHere.append("Now!")
            arTblHere.append("　")
        case "2":
            arTblHere.append("　")
            arTblHere.append("　")
            arTblHere.append("Now!")
        default: break
        }


        lblTittle.text = arBaseData[0]
        //現在工程
        var strTmpProcess = ""
        var strNowProcess = ""
        strTmpProcess = arBaseData[5]
        strNowProcess = koteiCDtoJP(strTmpProcess)!
        lblNowProcess.text = strNowProcess
        var strNowState = ""
        var strNowProcessSdate = ""
        var strNowProcessEdate = ""
        
        switch strTmpProcess {
        case "0":
            strNowState = jotaiCDtoJP(arBaseData[15])!
            strNowProcessSdate = arBaseData[6]
            strNowProcessEdate = arBaseData[7]
        case "1":
            strNowState = jotaiCDtoJP(arBaseData[16])!
            strNowProcessSdate = arBaseData[8]
            strNowProcessEdate = arBaseData[9]
        case "2":
            strNowState = jotaiCDtoJP(arBaseData[17])!
            strNowProcessSdate = arBaseData[10]
            strNowProcessEdate = arBaseData[11]
         default: break
         }

         arTblImpProcessMain = arTblImpProcess
         arTblStartDateMain = arTblStartDate
         arTblEndDateMain = arTblEndDate
         arTblDaysMain = arTblDays
         arTblStateMain = arTblState
         arTblHereMain = arTblHere
        
        //現在状態
        lblNowState.text = strNowState
        //現在工程開始日
        lblNowProcessSdate.text = strNowProcessSdate
        //現在工程終了日
        lblNowProcessEdate.text = strNowProcessEdate

        //let point = CGPointMake(self.view.frame.width/4, self.view.frame.height/4)

        let pi = CGFloat(M_PI)
        let start:CGFloat = -1*pi/2 // 開始の角度
//        let end :CGFloat = pi*2/12*2 // 終了の角度
        
        let preAngle = jotaiCDtoPerc(arBaseData[15])
        let crtAngle = jotaiCDtoPerc(arBaseData[16])
        let tesAngle = jotaiCDtoPerc(arBaseData[17])
        var nowAngle :CGFloat
        switch strTmpProcess {
        case "0":
            nowAngle = preAngle!
        case "1":
            nowAngle = crtAngle!
        case "2":
            nowAngle = tesAngle!
        default:
            nowAngle = 0.0
        }
        let nowEnd :CGFloat = 1.0 * pi * 2.0 - (pi / 2.0) // 終了の角度
        let nowEnd1 :CGFloat = nowAngle * pi * 2.0 - (pi / 2.0) // 終了の角度
        //
        let nowBpath0: UIBezierPath = UIBezierPath()
        let nowBpath1: UIBezierPath = UIBezierPath()
        let nowBpath2: UIBezierPath = UIBezierPath()
        nowBpath0.moveToPoint(CGPointMake(self.view.frame.width/2, 190))
        nowBpath0.addArcWithCenter(CGPointMake(self.view.frame.width/2, 190), radius: 78, startAngle: start, endAngle: nowEnd, clockwise: true) // 円弧
        nowBpath1.moveToPoint(CGPointMake(self.view.frame.width/2, 190))
        nowBpath1.addArcWithCenter(CGPointMake(self.view.frame.width/2, 190), radius: 90, startAngle: start, endAngle: nowEnd1, clockwise: true) // 円弧
        nowBpath2.moveToPoint(CGPointMake(self.view.frame.width/2, 190))
        nowBpath2.addArcWithCenter(CGPointMake(self.view.frame.width/2, 190), radius: 90, startAngle: start, endAngle: nowEnd, clockwise: true) // 円弧
        let nowLayer0 = CAShapeLayer()
        let nowLayer1 = CAShapeLayer()
        let nowLayer2 = CAShapeLayer()

        nowLayer0.fillColor = UIColor.whiteColor().CGColor
        nowLayer0.path = nowBpath0.CGPath
        nowLayer0.zPosition = -0.5

        nowLayer1.fillColor = UIColor.greenColor().CGColor
        nowLayer1.path = nowBpath1.CGPath
        nowLayer1.zPosition = -0.9
        
        nowLayer2.fillColor = UIColor.grayColor().CGColor
        nowLayer2.path = nowBpath2.CGPath
        nowLayer2.zPosition = -1.0
    
        self.view.layer.addSublayer(nowLayer0)
        self.view.layer.addSublayer(nowLayer1)
        self.view.layer.addSublayer(nowLayer2)
        
        
    }
    
    //セル数を設定する
    func tableView(tblProcessInf: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arTblProcess.count
    }
    //各セルの要素を設定する
    func tableView(tblProcessInf: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tblProcessInf.dequeueReusableCellWithIdentifier("processCell", forIndexPath: indexPath) as! stsTableViewCell3
        cell.lblProcess.text = arTblProcess[indexPath.row]
        cell.lblImpProcess.text = arTblImpProcessMain[indexPath.row]
        cell.lblStartDate.text = arTblStartDateMain[indexPath.row]
        cell.lblEndDate.text = arTblEndDateMain[indexPath.row]
        cell.lblDays.text = arTblDaysMain[indexPath.row]
        cell.lblState.text = arTblStateMain[indexPath.row]
        cell.lblNowHere.text = arTblHereMain[indexPath.row]
        return cell
    }
    func tableView(tblProcessInf: UITableView,
        willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
            return indexPath
    }
    
    func tableView(tblProcessInf: UITableView,
        didSelectRowAtIndexPath indexPath: NSIndexPath) {
    }
    //ラベルタッチ
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event)
        for touch: UITouch in touches {
            let tag = touch.view!.tag
            switch tag {
            case intLabelTag1:
                let title = "進捗更新"
               // let rowValue = arDates[indexPath.row]
                let message = "進捗を更新してください \n\n\n\n\n\n\n\n\n"
                let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
                let okAction = UIAlertAction(title: "変更", style: UIAlertActionStyle.Default,handler:{
                    (action: UIAlertAction!) -> Void in
                    
                    if self.selChangeState != ""{
                        self.lblNowState.text = self.selChangeState
//                        let updJotai = self.jotaiJPtoCD(self.selChangeState)
                       // self.arStss[indexPath.row] = updJotai!
                    }
                    else{
                    //    self.arStss[indexPath.row] = "1"
                    }
                    //self.tbvStatus.reloadData()
                })
                let cancelAction = UIAlertAction(title: "キャンセル", style: .Cancel) { action in}

                // PickerView
                pvChangeState.selectRow(1, inComponent: 0, animated: true)
                pvChangeState.frame = CGRectMake(0, 50, view.bounds.width * 0.65, 150)
                pvChangeState.dataSource = self
                pvChangeState.delegate = self
                alert.view.addSubview(pvChangeState)
                alert.addAction(okAction)
                alert.addAction(cancelAction)
                presentViewController(alert, animated: true, completion: nil)

                print("are")
            default:
                break
                
            }
            
        }
    }
    
    // PickerViewの列数
    func numberOfComponentsInPickerView(pvChangeState: UIPickerView) -> Int {
        return 1
    }
    
    // PickerViewの行数
    func pickerView(pvChangeState: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arStateItems.count
    }
    
    // PickerViewの項目
    func pickerView(pvChangeState: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arStateItems[row]
    }
    
    // PickerViewの項目選択時
    func pickerView(pvChangeState: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selChangeState = arStateItems[row]
    }

    
    // 状態をコード値から円グラフ値に変換する
    func jotaiCDtoPerc(JotaiCD: String) -> CGFloat? {
        let Jotai = JotaiCD
        var fltJotai: CGFloat
        switch Jotai {
        case "0":
            fltJotai = 0.0
        case "1":
            fltJotai = 0.1
        case "2":
            fltJotai = 0.25
        case "3":
            fltJotai = 0.5
        case "4":
            fltJotai = 0.75
        case "5":
            fltJotai = 0.9
        case "6":
            fltJotai = 1.0
        default:
            fltJotai = 0.0
        }
        return fltJotai
    }
    // 状態をコード値から日本語に変換する
    func jotaiCDtoJP(JotaiCD: String) -> String? {
        let Jotai = JotaiCD
        var JotaiJP: String = ""
        switch Jotai {
        case "0":
            JotaiJP = "０％"
        case "1":
            JotaiJP = "１０％"
        case "2":
            JotaiJP = "２５％"
        case "3":
            JotaiJP = "５０％"
        case "4":
            JotaiJP = "７５％"
        case "5":
            JotaiJP = "９０％"
        case "6":
            JotaiJP = "完了！"
        default:
            JotaiJP = "なし"
        }
        return JotaiJP
    }
    // 状態をコード値から日本語に変換する
    func jotaiJPtoCD(JotaiJP: String) -> String? {
        let Jotai = JotaiJP
        var JotaiCD: String = ""
        switch Jotai {
        case "０％":
            JotaiCD = "0"
        case "１０％":
            JotaiCD = "1"
        case "２５％":
            JotaiCD = "2"
        case "５０％":
            JotaiCD = "3"
        case "７５％":
            JotaiCD = "4"
        case "９０％":
            JotaiCD = "5"
        case "完了！":
            JotaiCD = "6"
        default:
            JotaiCD = "0"
        }
        return JotaiCD
    }
    
    // 工程をコード値から日本語に変換する
    func koteiCDtoJP(koteiCD: String) -> String? {
        let kotei = koteiCD
        var koteiJP: String = ""
        switch kotei {
        case "0":
            koteiJP = "準備"
        case "1":
            koteiJP = "作成"
        case "2":
            koteiJP = "テスト"
        default:
            koteiJP = ""
        }
        return koteiJP
    }
    // 工程をコード値から日本語に変換する
    func koteiJPtoCD(koteiJP: String) -> String? {
        let kotei = koteiJP
        var koteiCD: String = ""
        switch kotei {
        case "準備":
            koteiCD = "0"
        case "作成":
            koteiCD = "1"
        case "テスト":
            koteiCD = "2"
        default:
            koteiCD = "0"
        }
        return koteiCD
    }

    override func viewWillAppear(animated: Bool) {
        tblProcessInf.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
