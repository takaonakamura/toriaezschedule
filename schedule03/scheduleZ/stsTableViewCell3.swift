//
//  stsTableViewCell3.swift
//  scheduleZ
//
//  Created by 中村考男 on 2016/03/14.
//  Copyright © 2016年 tamagawa. All rights reserved.
//

import UIKit

class stsTableViewCell3: UITableViewCell {

    @IBOutlet weak var lblProcess: UILabel!
    @IBOutlet weak var lblImpProcess: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblDays: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblNowHere: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
