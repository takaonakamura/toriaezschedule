//
//  stsTabController02.swift
//  scheduleZ
//
//  Created by 中村考男 on 2016/04/09.
//  Copyright © 2016年 tamagawa. All rights reserved.
//

import UIKit
import RealmSwift

class stsTabController02: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tblPastSchedule: UITableView!
    var arTblTittle = [String]()
    var arTblStartDateMain = [String]()
    var arTblEndDateMain = [String]()
    var arTblDaysMain = [String]()
    
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //Realm
        let realm = try! Realm()
        let hisData = realm.objects(scheduleHistRealm)
        for hisDatas in  hisData{
            arTblTittle.append(hisDatas.Tittle)
            arTblStartDateMain.append(hisDatas.StartDate)
            arTblEndDateMain.append(hisDatas.EndDate)
            arTblDaysMain.append(hisDatas.Days)
        }
        
        
    }
    //セル数を設定する
    func tableView(tblPastSchedule: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arTblTittle.count
    }
    //各セルの要素を設定する
    func tableView(tblPastSchedule: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tblPastSchedule.dequeueReusableCellWithIdentifier("pastCell", forIndexPath: indexPath) as! stsTableViewCell5
        cell.lblTittle.text = arTblTittle[indexPath.row]
        cell.lblStartDate.text = arTblStartDateMain[indexPath.row]
        cell.lblEndDate.text = arTblEndDateMain[indexPath.row]
        cell.lblDays.text = arTblDaysMain[indexPath.row]
        
        return cell
    }
    func tableView(tblPastSchedule: UITableView,
                   willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        return indexPath
    }
    
    func tableView(tblPastSchedule: UITableView,
                   didSelectRowAtIndexPath indexPath: NSIndexPath) {
    }
    
    override func viewWillAppear(animated: Bool) {
        tblPastSchedule.reloadData()
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
