//
//  stsTableViewCell5.swift
//  scheduleZ
//
//  Created by 中村考男 on 2016/04/09.
//  Copyright © 2016年 tamagawa. All rights reserved.
//


import UIKit

class stsTableViewCell5: UITableViewCell {

    @IBOutlet weak var lblTittle: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblDays: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
