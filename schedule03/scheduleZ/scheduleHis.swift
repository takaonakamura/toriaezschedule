//
//  scheduleHis.swift
//  scheduleZ
//
//  Created by 中村考男 on 2016/04/13.
//  Copyright © 2016年 tamagawa. All rights reserved.
//

import RealmSwift

class scheduleHis: Object {
    
    dynamic var id = 0
    dynamic var title = ""
    dynamic var startDate = ""
    dynamic var endDate = ""
    override static func primaryKey() -> String?{
        return id
    }

}
